import { createECDH } from 'crypto';
import fs from 'fs'
import jwt from 'jsonwebtoken'
import JWT, { ES256_keys } from './JWT.js'

/**
 * #### Do not import in front use  {@link enerfox_common/utils/JWTBack.default} instead
 * Utility to help create private/public key (prime256v1 pem format) and sign JWT token
 */
export default class JWTBack extends JWT{

    private static keyPath = "./ES256_keys/"
    private static privateKey: string;
    public static publicKey: string;

    /**
     * Load if exist or generate into {@link privateKey} & {@link publicKey} in pem format
     */
    private static loadKeys() {
        if (!fs.existsSync(JWTBack.keyPath))
            this.keyGen()
        if(fs.existsSync(JWTBack.keyPath+'secret.pem'))
            JWTBack.privateKey = fs.readFileSync(JWTBack.keyPath+'secret.pem').toString();
        if(fs.existsSync(JWTBack.keyPath+'secret.pem'))
            JWTBack.publicKey = fs.readFileSync(JWTBack.keyPath+'pub.pem').toString();
    }

    /**
     * Generate and return ES256 keys in folder {@link keyPath}
     */
    public static keyGen(): ES256_keys {
        let keys = createECDH('secp256k1')
        keys.generateKeys()

        let private_key_pem = "-----BEGIN PRIVATE KEY-----\n"
                            + Buffer.from(`308184020100301006072a8648ce3d020106052b8104000a046d306b0201010420${keys.getPrivateKey('hex')}a144034200${keys.getPublicKey('hex')}`, 'hex').toString('base64')
                            + "\n-----END PRIVATE KEY-----"
        let public_key_pem = "-----BEGIN PUBLIC KEY-----\n"
                            + Buffer.from(`3056301006072a8648ce3d020106052b8104000a034200${keys.getPublicKey('hex')}`, 'hex').toString('base64')
                            + "\n-----END PUBLIC KEY-----"

        if (!fs.existsSync(JWTBack.keyPath))
            fs.mkdirSync(JWTBack.keyPath)

        fs.writeFileSync(JWTBack.keyPath+'secret.pem', private_key_pem, {flag: 'w'});
        fs.writeFileSync(JWTBack.keyPath+'pub.pem', public_key_pem, {flag: 'w'});

        JWTBack.privateKey = private_key_pem
        JWTBack.publicKey = public_key_pem

        return {private_key: private_key_pem, public_key: public_key_pem}
    }

    
    //TODO manage expiration date of token
    /**
     * 🚩 This JWT is NOT encrypted only signed.
     * @return a Signed JWT token.
     */
    public static sign(json: any): string {
        if(!JWTBack.privateKey)
            JWTBack.loadKeys()
            
        return jwt.sign(json, JWTBack.privateKey, { algorithm: 'ES256' });
    }
}