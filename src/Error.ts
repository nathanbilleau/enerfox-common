import {Response} from "express";

/**
 * Utility to help manage error response.
 * How to use ?
 * Throw or return a ServerError object at the end use handleError to send a response with status and user readable message.
 */
export default class ServerError implements Error{
    public name:string = "";
    public error:string;
    public message:string;
    public status:number;

    /**
     * @param error - Technical error
     * @param status - Http status
     * @param message - Redable error, if not set same as error
     */
    constructor(error:string, status:number=500, message?:string) {
        this.error = error;
        this.message = error;
        this.status = status;
        if(message)
            this.message = message;
    }

    public toString():string {
        return this.error;
    }
}

/**
 * #### Handle error depending on if it is or not a ServerError object.
 * @param err - The error
 * @param response - Express Response object
 * @return Promise
 */
export function handleError(err:ServerError|any, response:Response){
    console.error(err);
    if (!err.message || !err.status){
        let message = "Server error"
        if (err.message)
            message = err.message
        err = new ServerError(err, 500, message)
    }
    response.status(err.status).json({message: err.message})
}