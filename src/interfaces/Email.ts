export interface IContactAttributes {
    SOCIETE?: string;
    ACTIVITE_ENTREPRISE?: number;
    PRENOM?: string;
    NOM?: string;
    TELEPHONE?: string;
    FONCTION?: string;
    TITRE?: "Monsieur" | "Madame" | "Autre";
}

export interface IBodyCreateAccount {
    email: string;
    updateEnabled?: boolean; // enable the upsert
    attributes?: IContactAttributes;
}

export interface IBodyPushLabelInCategory{
    category: string;
    label: string;
}

export interface ITo {
    email: string;
    name?: string;
}

export interface IBodyResetPassword {
    to: ITo;
    link: string;
}

export interface IBodyAccountActivation {
    to: ITo;
    code: string;
}

export interface IBodyCreateCompany {
    to: ITo;
}

export interface IBodyJoinCompany {
    to: ITo;
    companyName: string;
}

export interface IBodyJoinYourCompanyDemand {
    to: ITo;
}

export interface IBodyJoinYourCompanyDemandRevival {
    to: ITo;
}

export interface IBodyJoinCompanyRefused {
    to: ITo;
    companyName: string;
}

export interface IBodyJoinCompanyAccepted {
    to: ITo;
    companyName: string;
}

export type AttributeCategory = "normal" | "transactional" | "category" | "calculated" | "global"