import { ICompany } from "./Company";

export enum Gender {
    MALE = 'M',
    FEMALE = 'F',
    OTHER = 'NC'
}

export interface INewUser {
    email: string;
    gender: Gender;
    firstname: string;
    lastname: string;
    phone: string;
    password: string;
}

export interface IUser extends Omit<INewUser, "password"> {
    uuid: string;
    activated: number;
    activation_code?: string;
    waitingApproval?: string;
}

export interface IJWTContent {
    uuid: string;
    displayName: string;
    company_id: string;
    activated: number;
}

export interface IBodyLogin {
    email: string;
    password: string;
}

export interface IBodyCreateAccount {
    email: string;
    password: string;
    uuid: string;
    gender: Gender;
    firstname: string;
    lastname: string;
    phone: string;
}

export interface IBodyJoinCompany {
    uuid: string;
    SIRET: string;
}

export interface IBodyCreateCompany {
    uuid: string;
    company: ICompany;
}

export interface IBodyActivate {
    activation_code: string;
}

export interface IBodyResetPassword {
    uuid: string;
    hash: string;
    password: string;
}

export interface IBodySendResetPasswordLink {
    email: string;
}