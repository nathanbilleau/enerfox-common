export interface IAddress {
    streetAndNumber: string;
    postalCode: string;
    city: string;
    complement?: string;
}

export interface ICompany {
    SIRET: string;
    name: string;
    address: IAddress;
    businessSector: string;
}