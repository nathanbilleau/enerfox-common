import DiscordTransport from 'winston-discord-transport'
import { createLogger, transports, format } from 'winston'
const { combine, timestamp, printf } = format

const { name } = require('../../package.json')

const discordHook = {
    prod: '',
    dev: ''
}

const logFormat = printf(({ level, message, timestamp }) => {
    return `${timestamp} ${level}: ${message}`;
});

const timezoned = () => {
    return new Date().toLocaleString('fr-FR', {
        timeZone: 'Europe/Paris'
    });
}

const logger = createLogger({
    levels: {
        crit: 0,
        error: 1,
        warn: 2,
        debug: 3,
        info: 4,
    },
    format: combine(
        timestamp({
            format: timezoned
        }),
        logFormat
    ),
    transports: [
        new transports.Console(),
        new transports.File({ filename: './logs/info.txt', maxFiles: 5, maxsize: 10000000, level: 'info' }),
        new transports.File({ filename: './logs/warn.txt', maxFiles: 5, maxsize: 10000000, level: 'warn' }),
        new transports.File({ filename: './logs/error.txt', maxFiles: 5, maxsize: 10000000, level: 'error' }),
        new DiscordTransport({
            webhook: discordHook.dev,
            level: 'error',
            defaultMeta: {
                service: name
            }
        })
    ]
})

export default logger