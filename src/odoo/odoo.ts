import fetch from 'node-fetch'
import { URLSearchParams } from "url"
import {
    Domain,
    Fields,
    ICompany,
    ICompanyCreate,
    IContactCreate,
    IContact,
    ICategory,
    ContactCategoryPrefix,
    IContactFind,
    ICompanyFind,
    IEdit,
    IEditContactCategory,
    ICategoryEdit,
    IIndustry
} from './interfaces'

import contactFunctions from './contactFunctions'

export type {
    Domain,
    Fields,
    ICompany,
    ICompanyCreate,
    IContactCreate,
    IContact,
    ICategory,
    IContactFind,
    ICompanyFind,
    IEdit,
    IEditContactCategory,
    ICategoryEdit,
}

export {
    ContactCategoryPrefix, contactFunctions
}


export default class Odoo {
    private token: Promise<string>
    private clientKey = ''
    private clientSecret = ''
    private baseUrl = ''

    constructor({ url,
        clientKey, clientSecret }: { url: string, clientKey: string, clientSecret: string }) {
        this.clientKey = clientKey
        this.clientSecret = clientSecret
        this.baseUrl = url

        this.token = this.authenticate()
    }

    // TODO automatically request a new token if the current one is expired
    // Authenticate with the server
    // Return a promise with the token
    private async authenticate(): Promise<string> {
        const basicToken = Buffer.from(`${this.clientKey}:${this.clientSecret}`).toString('base64')

        return await fetch(this.baseUrl + '/api/authentication/oauth2/token', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Basic ${basicToken}`,
            },
            body: new URLSearchParams({
                'grant_type': 'client_credentials',
                'scope': 'all',
            }).toString(),
        })
            .then(res => res.json())
            // @ts-ignore
            .then(res => res.access_token)
    }


    /**
     * 
     * @param fields Fields as an array
     * @returns Fields as a string
     * 
     * @example
     * odoo.formatFields(['firstname', 'lastname', 'email', 'city'])
     * // ['firstname','lastname','email','city']
     */
    private formatFields = (fields: Fields): string => {
        return `[${fields.map(field => `'${field}'`).join(',')}]`
    }


    /**
     * 
     * @param domain Domain as an array of arrays
     * @returns Domain as a string
     * 
     * @example
     * odoo.formatDomain([['firstname', '=', 'Nicolas']])
     * // returns '[('firstname','=','Nicolas')]'
     */
    private formatDomain = (domain: Domain): string => {
        return `[${domain.map(item => `(${item.map(i => `'${i}'`).join(',')})`).join(',')}]`
    }


    /**
     * 
     * @param siret Siret of the company
     * @returns Siret of the company without spaces and dashes
     */
    private formatSiret = (siret: string): string => {
        return siret?.replace(/\s/g, '')?.replace(/-/g, '')
    }


    /**
     * 
     * @param params.domain Domain to filter the results
     * @param params.fields Fields to return 
     * @returns [{id: id, fields: value...}]
     * 
     * @example
     * odoo.getContacts({ fields: ['firstname', 'lastname', 'email', 'city'], domain: [['firstname', '=', 'Nicolas']] })
     */
    public async getContacts({ domain = [], fields = [] }: { domain?: Domain, fields?: Fields }): Promise<IContact[]> {
        domain.push(['is_company', '!=', 'true'])
        fields.push('email')

        let url = this.baseUrl + `/api/search_read/res.partner/999999/id/?`
        url += `&fields=${this.formatFields(fields)}`

        if (domain) {
            url += `&domain=${this.formatDomain(domain)}`
        }

        if (fields) {
            url += `&fields=${this.formatFields(['email', ...fields])}`
        }

        return new Promise(async (resolve, reject) => {
            fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${await this.token}`,
                }
            })
                .then(res => res.json())
                .then(res => {
                    const contacts = (res as IContact[]).map(this.newContactObject)
                    resolve(contacts)
                })
                .catch(reject)

        })
    }


    /**
     * 
     * Get contacts categories (with or without prefix)
     * 
     * @param {ContactCategoryPrefix} [prefix] Prefix to filter the results
     * @example
     * odoo.getContactsCategories()
     * // returns [{id: id, display_name: display_name }]
     * 
     * @returns {ContactCategory}
     */
    public async getCategories({ prefix }: { prefix?: ContactCategoryPrefix }): Promise<ICategory[]> {
        const fields = ['id', 'name']
        // FIXME prefix domain not working, the filter is ignored
        const prefixFilter = prefix ? `&domain=${this.formatDomain([['name', '=ilike', prefix]])}` : ''

        return await fetch(this.baseUrl + `/api/search_read/res.partner.category/999999/id/?fields=${this.formatFields(fields)}${prefixFilter}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${await this.token}`,
            }
        })
            .then(res => res.json()) as ICategory[]

    }

    /**
     * Create a contact in Odoo
     * 
     * @param contact Contact
     * 
     * @returns Contact
     */
    public async createContact(contact: IContactCreate, checkEmailUnique = true): Promise<IContact> {
        const companyId = contact.company?.id
        delete contact.company

        const values = {
            ...contact,
            is_company: false,
            parent_id: companyId
        }

        if (checkEmailUnique)
            checkEmailUnique = !(await this.findContact(contact)).valueOf()

        console.log(checkEmailUnique)
        console.log((await this.findContact(contact)))
        console.log(!(await this.findContact(contact)))

        if (checkEmailUnique)
            return new Promise(async (resolve, reject) => {
                fetch(this.baseUrl + `/api/create/res.partner?values=${JSON.stringify(values)}`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${await this.token}`,
                    }
                })
                    .then(res => res.json())
                    .then(res => {
                        const id = (res as IContact['id'][])[0]

                        // @ts-ignore
                        resolve(this.newContactObject({ id, ...contact }))
                    })
                    .catch(reject)
            })
        else
            return Promise.reject("User already exist")
    }


    /**
     * Find one by email and/or id and filter fields
     */
    public async findContact({ email, id, fields }: IContactFind): Promise<IContact> {
        const domain: Domain = [['is_company', '!=', 'true']]
        if (email) {
            domain.push(['email', '=', email])
        }
        if (id) {
            domain.push(['id', '=', id])
        }
        let url = this.baseUrl + `/api/search_read/res.partner?domain=${this.formatDomain(domain)}`

        if (fields) {
            url += `&fields=${this.formatFields(['id', 'email', ...fields])}`
        }

        return new Promise(async (resolve, reject) => {
            fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${await this.token}`,
                }
            })
                .then(res => res.json())
                .then(res => {
                    if (res === []) {
                        reject('Contact not found')
                    } else {
                        const contact = (res as IContact[])[0]
                        resolve(this.newContactObject(contact))
                    }
                })
                .catch(() => {
                    reject('Contact not found')
                })
        })
    }


    /**
     * Create a company
     * 
     * @param company Company
     * 
     * @returns Company
     * 
     */

    public async createCompany(company: ICompanyCreate): Promise<ICompany> {
        const values = {
            ...company,
            siret: company.siret && this.formatSiret(company.siret),
            is_company: true
        }
        return new Promise(async (resolve, reject) => {
            fetch(this.baseUrl + `/api/create/res.partner?values=${JSON.stringify(values)}`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${await this.token}`,
                }
            })
                .then(res => res.json())
                .then(res => {
                    const id = (res as ICompany['id'][])[0]

                    resolve({
                        id,
                        name: company.name,
                        siret: company.siret,
                        email: company.email,
                        edit: (values) => {
                            this.edit({ id, values })
                        }
                    })
                })
                .catch(reject)
        })
    }


    /**
     * 
     * Find a company by siret
     * 
     * @param param.siret Siret of the company
     * @returns Company 
     */
    public async findCompany({ id, siret, fields }: ICompanyFind): Promise<ICompany> {
        const siretDomain: Domain = [['is_company', '=', 'true']]
        if (siret) {
            siretDomain.push(['siret', '=', this.formatSiret(siret)])
        }
        if (id) {
            siretDomain.push(['id', '=', id])
        }
        let url = this.baseUrl + `/api/search_read/res.partner?domain=${this.formatDomain(siretDomain)}`

        if (fields) {
            url += `&fields=${this.formatFields(['id', 'siret', ...fields])}`
        }

        return new Promise(async (resolve, reject) => {
            fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${await this.token}`,
                }
            })
                .then(res => res.json())
                .then(res => {
                    if (!res) {
                        reject(new Error('Company not found'))
                    } else {
                        const company = (res as ICompany[])[0]
                        resolve({
                            ...company,
                            siret: company?.siret && this.formatSiret(company?.siret),
                            edit: (values) => {
                                this.edit({ id: company.id, values })
                            }
                        })
                    }
                })
                .catch(() => {
                    reject('Company not found')
                })
        })
    }

    public async getCompanies({ domain = [], fields = [] }: { domain?: Domain, fields?: Fields }): Promise<ICompany[]> {
        let url = this.baseUrl + `/api/search_read/res.partner/999999/id/?`
        domain.push(['is_company', '=', 'true'])
        fields.push('siret')

        if (fields) {
            url += `&fields=${this.formatFields(fields)}`
        }

        return new Promise(async (resolve, reject) => {
            fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${await this.token}`,
                }
            })
                .then(res => res.json())
                .then(res => {
                    const companies = (res as ICompany[]).map(company => ({
                        ...company,
                        siret: company?.siret && this.formatSiret(company.siret),
                        edit: (values: any) => {
                            this.edit({ id: company.id, values })
                        }
                    }))

                    resolve(companies)
                })
                .catch(reject)
        })
    }

    public async getIndustries(): Promise<IIndustry[]> {
        const fields: Fields = ['name', 'full_name', 'display_name']
        return new Promise(async (resolve, reject) => {
            fetch(this.baseUrl + `/api/search_read/res.partner.industry/999999/id/?fields=${this.formatFields(fields)}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${await this.token}`,
                }
            })
                .then(res => res.json())
                .then(res => {
                    const industries = (res as IIndustry[]).map(industry => ({
                        id: industry.id,
                        name: industry.name,
                        display_name: industry.display_name,
                        full_name: industry.full_name
                    }))

                    resolve(industries)
                })
                .catch(reject)
        })
    }

    private async edit({ id, values }: IEdit): Promise<any> {
        return await fetch(this.baseUrl + `/api/write/res.partner?ids=[${id}]&values=${JSON.stringify(values)}`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${await this.token}`
            }
        })
        .then(res => res.json())

    }

    private async addContactCategory({ id, categories }: IEditContactCategory): Promise<Response> {
        const categoriesToAdd = categories.map(cat => [4, cat.id, 0])
        return await this.edit({ id, values: { category_id: categoriesToAdd } })
    }

    private async removeContactCategory({ id, categories }: IEditContactCategory): Promise<Response> {
        const categoriesToRemove = categories.map(cat => [3, cat.id, 0])
        return await this.edit({ id, values: { category_id: categoriesToRemove } })
    }

    private newContactObject(contact: IContact): IContact {
        return {
            ...contact,
            edit: (values) => {
                this.edit({ id: contact.id, values })
            },
            categories: {
                add: async (categories: ICategoryEdit[]) => {
                    return await this.addContactCategory({ id: contact.id, categories })
                },
                remove: async (categories: ICategoryEdit[]) => {
                    return await this.removeContactCategory({ id: contact.id, categories })
                }
            }
        }
    }
}