export type DomainOperator = '=' | '!=' | '>' | '<' | '>=' | '<=' | '=like' | 'like' | '=ilike' | 'ilike' | 'not ilike' | 'in' | 'not in' | 'child_of' | 'parent_of' | '&' | '|' | '!'
export type Domain = [string, DomainOperator, any][]

export type Fields = string[]

export interface ICategory {
    id: number
    name: string
}

export interface ICategoryEdit {
    id: ICategory['id']
    name?: ICategory['name']
}

export enum ContactCategoryPrefix {
    STADE_COMMERCIAL = 'D-',
    POSITION_ENERFOX = 'P-'
}

interface IBaseContact {
    email: string
    edit(values: any): void
    phone?: string
    mobile?: string
    website?: string
    street?: string
    [key: string]: any
}

export interface IContact extends IBaseContact {
    id: number
    firstname?: string
    lastname?: string
    function?: string,
    company?: ICompany
    category_id: number[]
    categories: {
        add: (categories: ICategoryEdit[]) => void
        remove: (categories: ICategoryEdit[]) => void
    }
}


export interface IContactCreate extends IBaseContact {
    firstname?: string
    lastname?: string
    company?: ICompany
    customer?: boolean
    supplier?: boolean
}

export interface IContactFind extends Partial<Pick<IContact, 'id' | 'email'>> {
    fields?: Fields
}

export interface ICompany extends IBaseContact {
    id: number
    siret: string
    name: string
    industry_id?: number[]
}

export interface ICompanyCreate extends Omit<ICompany, 'id'> {}

export interface ICompanyFind extends Partial<Pick<ICompany, 'id' | 'siret' >> {
    fields?: Fields
}

export interface IEdit {
    id: IContact['id']
    values: any
}

export interface IEditContactCategory {
    id: IContact['id']
    categories: ICategoryEdit[]
}

export interface IIndustry {
    id: number
    name: string
    full_name: string
    display_name: string
}