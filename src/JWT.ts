import jwt from 'jsonwebtoken'
import { IJWTContent } from './interfaces/User';

export interface ES256_keys {
    private_key?: string
    public_key: string
}

/**
 * #### Backend version {@link enerfox_common/utils/JWTBack.default}
 * Utility to help read JWT token
 */
export default class JWT {

    /**
     * #### Mandatory to use {@link verify}
     */
    public static publicKey: string;

    /**
     * Read token content if it's not expired and have a valide signature 
     */
    public static verify(token: string): boolean {
        if (!JWT.publicKey) {
            console.error("JWT utils: You need to set a public key")
            return false
        } else {
            try {
                jwt.verify(token, JWT.publicKey);
                return true
            } catch (err) {
                return false
            }
        }
    }

    /**
     * #### 🚩 Doesn't check token signature or expiration date
     * Read token content without checking 
     */
    public static decode(token: any): IJWTContent {
        return jwt.decode(token) as IJWTContent;
    }
}