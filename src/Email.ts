//@ts-ignore
import * as SendinBlueSDK from '@sendinblue/client';
import { AttributeCategory } from './interfaces/Email'

const API_KEYS = {
  prod: '',
  dev: ''
}

const API_KEY = API_KEYS.dev

const TransactionalEmailsApi = new SendinBlueSDK.TransactionalEmailsApi()
TransactionalEmailsApi.setApiKey(SendinBlueSDK.TransactionalEmailsApiApiKeys.apiKey, API_KEY)

const ContactsApi = new SendinBlueSDK.ContactsApi()
ContactsApi.setApiKey(SendinBlueSDK.ContactsApiApiKeys.apiKey, API_KEY)

const AttributesApi = new SendinBlueSDK.AttributesApi()
AttributesApi.setApiKey(SendinBlueSDK.AttributesApiApiKeys.apiKey, API_KEY)

const enum TEMPLATES {
  Activation = 107,
  ResetPassword = 62,
  CreateCompany = 108,
  JoinCompany = 110,
  JoinYourCompanyDemand = 109,
  JoinYourCompanyDemandRevival = 113,
  JoinCompanyRefused = 112,
  JoinCompanyAccepted = 111,
  ContactFormConfirmation = 0
}

export default class Email {

  private static sender:SendinBlueSDK.SendSmtpEmailSender = { name: 'Enerfox', email: 'donotreply@enerfox.fr' }
  public static enerfoxTo:SendinBlueSDK.SendSmtpEmailTo = { name: 'Enerfox', email: 'contact@enerfox.fr' }
  
  private static send (
    to: Array<SendinBlueSDK.SendSmtpEmailTo>,
    templateId: number,
    params: any = {}
  ): Promise<void> {
    let sendSmtpEmail = new SendinBlueSDK.SendSmtpEmail()
    sendSmtpEmail.sender = this.sender
    sendSmtpEmail.to = to
    sendSmtpEmail.templateId = templateId
    sendSmtpEmail.params = params

    return TransactionalEmailsApi.sendTransacEmail(sendSmtpEmail).then(
      (_: any) => Promise.resolve(),
      (err: any) => {
        console.error(err)
        return Promise.reject(err)
      }
    )
  }

  public static createAttribute(attributeCategory: AttributeCategory, attributeName: string, createAttribute: SendinBlueSDK.CreateAttribute): Promise<void> {
    return AttributesApi.createAttribute(attributeCategory, attributeName, createAttribute).then(
      (_: any) => Promise.resolve(),
      (err: any) => {
        console.error(err)
        return Promise.reject(err)
      }
    )
  }

  public static deleteAttribute(attributeCategory: AttributeCategory, attributeName: string): Promise<void> {
    return AttributesApi.deleteAttribute(attributeCategory, attributeName).then(
      (_: any) => Promise.resolve(),
      (err: any) => {
        console.error(err)
        return Promise.reject(err)
      }
    )
  }

  public static updateAttribute(attributeName: string, updateAttribute: SendinBlueSDK.UpdateAttribute, attributeCategory: "category" | "calculated" | "global" = "category"): Promise<void> {
    return AttributesApi.updateAttribute(attributeCategory, attributeName, updateAttribute).then(
      (_: any) => Promise.resolve(),
      (err: any) => {
        console.error(err)
        return Promise.reject(err)
      }
    )
  }

  public static upSertAccount(contact: SendinBlueSDK.CreateContact): Promise<void> {
    return ContactsApi.createContact(contact).then(
      (_: any) => Promise.resolve(),
      (err: any) => {
        console.error(err)
        return Promise.reject(err)
      }
    )
  }

  public static getAttributes(): Promise<SendinBlueSDK.GetAttributes> {
    return AttributesApi.getAttributes().then(
      attributes => Promise.resolve(attributes.body),
      (err: any) => {
        console.error(err)
        return Promise.reject(err)
      }
    )
  }

  public static async pushLabelInCategory(category: string, label: string): Promise<void> {
    const newIndex = ((await Email.getAttributes())?.attributes?.find((e: SendinBlueSDK.GetAttributesAttributes) => e?.name === category)?.enumeration?.length ?? 0) + 1;
    const attribute = {
      value: newIndex,
      label: label
    };
    const attributeCategory = {
      enumeration: [attribute]
    };
    return Email.updateAttribute(category, attributeCategory).then(
      (_: any) => Promise.resolve(),
      (err: any) => {
        console.error(err)
        return Promise.reject(err)
      }
    )
  }

  public static activation(
    to: SendinBlueSDK.SendSmtpEmailTo,
    code: string
  ): Promise<void> {
    return this.send([to], TEMPLATES.Activation, { code: code })
  }

  public static resetPassword(
    to: SendinBlueSDK.SendSmtpEmailTo,
    link: string
  ): Promise<void> {
    return this.send([to], TEMPLATES.ResetPassword, { link: link })
  }

  public static createCompany(
    to: SendinBlueSDK.SendSmtpEmailTo
  ): Promise<void> {
    return this.send([to], TEMPLATES.ResetPassword)
  }

  public static joinCompany(
    to: SendinBlueSDK.SendSmtpEmailTo,
    companyName: string
  ): Promise<void> {
    return this.send([to], TEMPLATES.ResetPassword, { entdemandee: companyName })
  }

  public static joinYourCompanyDemand(
    to: SendinBlueSDK.SendSmtpEmailTo
  ): Promise<void> {
    return this.send([to], TEMPLATES.JoinYourCompanyDemand)
  }

  public static joinYourCompanyDemandRevival(
    to: SendinBlueSDK.SendSmtpEmailTo
  ): Promise<void> {
    return this.send([to], TEMPLATES.JoinYourCompanyDemandRevival)
  }

  public static joinCompanyRefused(
    to: SendinBlueSDK.SendSmtpEmailTo,
    companyName: string
  ): Promise<void> {
    return this.send([to], TEMPLATES.JoinCompanyRefused, { entdemandee: companyName })
  }

  public static joinCompanyAccepted(
    to: SendinBlueSDK.SendSmtpEmailTo,
    companyName: string
  ): Promise<void> {
    return this.send([to], TEMPLATES.JoinCompanyAccepted, { entdemandee: companyName })
  }

  static contactFormConfirmation (
    to: SendinBlueSDK.SendSmtpEmailTo,
    companyName: string
  ):Promise<void> {
    return this.send([to], TEMPLATES.ContactFormConfirmation, {entdemandee: companyName})
  }
}